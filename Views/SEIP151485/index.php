<?php
use App\Person;
use App\Student;

function __autoload($className){
    echo $className."</br>";
    //we need to cut App\ from App\Person then we have to concatenate with ".php"
   list($ns,$cn)=explode ("\\",$className);
    require_once("../../src/BITM/SEIP151485/".$cn.".php");


}

$objPerson=new Person();
echo $objPerson->showPersonInfo();
$objStudent=new Student();
echo $objStudent->showStudentInfo();
?>