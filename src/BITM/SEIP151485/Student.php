<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/31/2016
 * Time: 10:13 AM
 */

namespace App;


class Student extends Person
{
    public $studentID = "SEIP151485";

    public function showStudentinfo()
    {
        parent::showPersonInfo();
        echo $this->studentID;

    }
}