<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/31/2016
 * Time: 10:13 AM
 */

namespace App;


class Person
{
public $name="Naznin";
    public $gender="Female";
    public $blood_group="AB+";
    public function showPersonInfo(){
        echo $this->name."<br/>";
        echo $this->gender."</br>";
        echo $this->blood_group."</br>";
    }
}